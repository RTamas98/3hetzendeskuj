const emailBox = document.querySelector('.main__input__one__line');
const emailFail = document.querySelector('.main__input__one__line--dp');
let emailTrue = false;
const  messageBox = document.querySelector('.main__input__message');
const  messageFail = document.querySelector('.main__input__message--dp');
let messageTrue = false;
const  firstNameBox = document.querySelector('.main__first');
const firstInvalidName = document.querySelector('.main__input__first--dp1');
const  firstNameFail = document.querySelector('.main__input__first--dp');
let firstNameTrue = false;
const  lastNameBox = document.querySelector('.main__last--inp');
const lastInvalidName = document.querySelector('.main__input__last--dp1');
const  lastNameFail = document.querySelector('.main__input__last--dp');
let lastNameTrue = false;
const  companyNameBox = document.querySelector('.main__company--inp');
const  companyNameFail = document.querySelector('.main__input__comp--dp');
let companyNameTrue = false;
const numberOfEmpBox = document.querySelector('.main__input--emp');
const numberOfEmpFail = document.querySelector('.main__input--emp--dp');
let numberOfEmpTrue = false;
const phoneNumberBox = document.querySelector('.main__input__phone');
const phoneNumberFail = document.querySelector('.main__input__phone--dp');
let phoneNumberTrue = false;
const chosenBoxY = document.querySelector('.main__checkY');
const chosenBoxN = document.querySelector('.main__checkN');
const chosenBoxFail = document.querySelector('.main__input__select--dp');
let chosenTrue = true;
const contactSales = document.querySelector('.main__button');



emailFail.style.display = 'none';
messageFail.style.display = 'none';
firstNameFail.style.display = 'none';
lastNameFail.style.display = 'none';
companyNameFail.style.display = 'none';
numberOfEmpFail.style.display = 'none';
phoneNumberFail.style.display = 'none';
chosenBoxFail.style.display = 'none';
firstInvalidName.style.display = 'none';
lastInvalidName.style.display = 'none';



emailBox.addEventListener('blur', () => {
    ValidateEmail(emailBox.innerText);
});
emailBox.addEventListener('focus',() => {
    if(emailTrue){
        emailBox.style.border = '2px solid #f00';
    }else {
        emailBox.style.border = '2px solid #0c8d8d';
    }
});

messageBox.addEventListener('blur', () => {
    MessageBoxCheck(messageBox.innerText);
});
messageBox.addEventListener('focus', () => {
    if(messageTrue){
        messageBox.style.border = '2px solid #f00';
    }else {
        messageBox.style.border = '2px solid #0c8d8d';
    }

});

firstNameBox.addEventListener('blur', () => {
    FirstNameCheck(firstNameBox.innerText);
});
firstNameBox.addEventListener('focus', () => {
    if(firstNameTrue){
        firstNameBox.style.border = '2px solid #f00';
    }else {
        firstNameBox.style.border =  '2px solid #0c8d8d';
    }

});

lastNameBox.addEventListener('blur', () => {
    LastNameCheck(lastNameBox.innerText);
});
lastNameBox.addEventListener('focus', () => {
    if(lastNameTrue){
        lastNameBox.style.border = '2px solid #f00';
    }else {
        lastNameBox.style.border = '2px solid #0c8d8d';
    }
});

companyNameBox.addEventListener('blur', () => {
    CompanyNameCheck(companyNameBox.innerText);
});
companyNameBox.addEventListener('focus', () => {
    if(companyNameTrue){
        companyNameBox.style.border = '2px solid #f00';
    }else {
        companyNameBox.style.border =  '2px solid #0c8d8d';
    }
});

numberOfEmpBox.addEventListener('blur', () => {
    NumberOfEmployeesCheck(numberOfEmpBox.innerText);
})

numberOfEmpBox.addEventListener('focus', () => {
    if(numberOfEmpTrue){
        numberOfEmpBox.style.border = '2px solid #f00';
    }else {
        numberOfEmpBox.style.border = '2px solid #0c8d8d';
    }
})



phoneNumberBox.addEventListener('blur', () => {
    ValidatePhone(phoneNumberBox.innerText);
});

phoneNumberBox.addEventListener('focus',() => {
   if(phoneNumberTrue){
       phoneNumberBox.style.border = '2px solid #f00';
   } else {
       phoneNumberBox.style.border = '2px solid #0c8d8d';
   }
});

chosenBoxY.addEventListener('click', () => {
    chosenBoxFail.style.display = 'none';
    chosenTrue = false;
});
chosenBoxN.addEventListener('click', () => {
    chosenBoxFail.style.display = 'none';
    chosenTrue = false;
});

contactSales.addEventListener('click', () => {
    ValidateEmail(emailBox.innerText);
    MessageBoxCheck(messageBox.innerText);
    FirstNameCheck(firstNameBox.innerText);
    LastNameCheck(lastNameBox.innerText);
    CompanyNameCheck(companyNameBox.innerText);
    NumberOfEmployeesCheck(numberOfEmpBox.innerText);
    ValidatePhone(phoneNumberBox.innerText);
    CheckBoxCheck(chosenTrue);
})


function ValidateEmail(inputText)
{
    let mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if(emailBox.value.match(mailformat))
    {
        emailBox.style.border = '1px solid #17494d';
        emailFail.style.display = 'none';
        emailTrue = false;
        return true;
    }
    else
    {
        emailBox.style.border = '1px solid #f00';
        emailFail.style.display = 'block';
        emailTrue = true;
        return false;
    }
}



function ValidatePhone(inputText){
    let phoneformat = /^[0-9]+$/;
    if(phoneNumberBox.value.match(phoneformat)){
        phoneNumberBox.style.border = '1px solid #17494d';
        phoneNumberFail.style.display = 'none';
        phoneNumberTrue = false;
        return true;
    }else {
        phoneNumberBox.style.border = '1px solid #f00';
        phoneNumberFail.style.display = 'block';
        phoneNumberTrue = true;
        return false;
    }
}

function MessageBoxCheck(inputText){
    if(messageBox.value.length == 0){
        messageFail.style.display = 'block';
        messageBox.style.border = '1px solid #f00';
        messageTrue = true;
    }
    else {
        messageFail.style.display = 'none';
        messageBox.style.border = '1px solid #17494d';
        messageTrue = false;
    }
}

function FirstNameCheck(inputText){
    let firstnameformat = /^[a-zA-Z*]+$/;
    if(firstNameBox.value.length === 0){
        firstNameFail.style.display = 'block';
        firstInvalidName.style.display = 'none';
        firstNameBox.style.border = '1px solid #f00';
        firstNameTrue = true;
        return true;
    }
    if((!firstNameBox.value.match(firstnameformat)) && firstNameBox.value.length > 0){
        firstInvalidName.style.display = 'block';
        firstNameFail.style.display = 'none';
        firstNameBox.style.border = '1px solid #f00';
        firstNameTrue = true;
        return true;
    }
    else {
        firstNameFail.style.display = 'none';
        firstInvalidName.style.display = 'none';
        firstNameBox.style.border = '1px solid #17494d';
        firstNameTrue = false;
        return false;

    }
}

function LastNameCheck(inputText){
    let lastnameformat = /^[a-zA-Z*]+$/;
    if(lastNameBox.value.length === 0){
        lastNameFail.style.display = 'block';
        lastInvalidName.style.display = 'none';
        lastNameBox.style.border = '1px solid #f00';
        lastNameTrue = true;
        return true;
    }
    if((!lastNameBox.value.match(lastnameformat)) && lastNameBox.value.length > 0){
        lastNameFail.style.display = 'none';
        lastInvalidName.style.display = 'block';
        lastNameBox.style.border = '1px solid #f00';
        lastNameTrue = true;
        return true;
    }
    else {
        lastNameFail.style.display = 'none';
        lastInvalidName.style.display = 'none';
        lastNameBox.style.border = '1px solid #17494d';
        lastNameTrue = false;
    }
}

function CompanyNameCheck(inputText){
    if(companyNameBox.value.length == 0){
        companyNameFail.style.display = 'block';
        companyNameBox.style.border = '1px solid #f00';
        companyNameTrue = true;
    }else {
        companyNameFail.style.display = 'none';
        companyNameBox.style.border = '1px solid #17494d';
        companyNameTrue = false;
    }
}

function NumberOfEmployeesCheck(inputText){
    if(numberOfEmpBox.selectedIndex == 0){
        numberOfEmpFail.style.display = 'block';
        numberOfEmpBox.style.border = '1px solid #f00';
        numberOfEmpTrue = true;
    }else {
        numberOfEmpFail.style.display = 'none';
        numberOfEmpBox.style.border = '1px solid #17494d';
        numberOfEmpTrue = false;
    }
}

function CheckBoxCheck(inputText){
    if(chosenTrue){
        chosenBoxFail.style.display = 'block';
    }
}


